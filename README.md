# Corner Case Technologies task

React App: a task given by Corner Case Technologies.

## Table of contents
- [Getting Started](#markdown-header-getting-started)
    * [Technologies](#markdown-header-technologies)
    * [Setup](#markdown-header-setup)
        * [Environment configuration](#markdown-header-environment-configuration)
- [Testing](#markdown-header-testing)

## Getting Started

### Technologies

* App creation:
    * React 16.12.0
    * Bootstrap 4.4
* Documentation: 
    * JSDoc 3.6
* Lint check:
    * ESLint 5.16
    
### Setup

To run this project, install it locally using npm:

```
npm install 
```

#### Environment configuration
**Important !**  
REACT_APP_DATA_SOURCE: If this variable is set to "default", the application will load information from the local constants file. In any other case information will be loaded according to API address set on REACT_APP_RADIO_STATIONS_FROM_SERVER.  

After installation to set environment variables run this command:

```
cp .env.example .env
```

And then to run the application:

```
npm start
```

## Testing

To run tests use npm and name of the file the test should be run on
*e.g.*

```
npm test Station.test.jsx
```


