import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import RADIO_STATIONS from './constants/constants';

import Top from './Top/Top';
import Station from './Station/Station';
import Bottom from './Bottom/Bottom';

require('dotenv').config();

/**
 * Class to create an App Component
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radioStations: [],
      selectedRadioStationId: null,
    };
  }

  componentDidMount() {
    this.setRadioStations();
  }

/**
 * To update radio stations according to information got
 */
setRadioStations = () => {
  if (process.env.REACT_APP_DATA_SOURCE === 'default') {
    this.setState({ radioStations: RADIO_STATIONS });
  } else {
    axios.get(process.env.REACT_APP_RADIO_STATIONS_FROM_SERVER)
      .then((res) => {
        this.setState({ radioStations: res.data });
      });
  }
};

/**
 * Handle change which radio station is selected
 * @param {object} radioStation - Object of the radio station selected
 */
selectRadioStation = (radioStation) => {
  const { radioStations, selectedRadioStationId } = this.state;
  const userSelectedRadioStationId = radioStations.find((s) => radioStation.id === s.id).id;

  this.setState({
    selectedRadioStationId: userSelectedRadioStationId
    === selectedRadioStationId ? null : userSelectedRadioStationId,
  });
};

render() {
  const { radioStations, selectedRadioStationId } = this.state;
  return (
    <div className="App">
      <Top />
      <div className="stations mt-3 pb-0" data-test="wrappingComponent">
        {radioStations.map((radioStation) => {
          const {
            id, name, frequency, img,
          } = radioStation;
          return (
            <Station
              name={name}
              frequency={frequency}
              key={id}
              img={img}
              onClick={() => this.selectRadioStation(radioStation)}
              show={selectedRadioStationId === id}
            />
          );
        })}
      </div>
      <Bottom
        show={selectedRadioStationId !== null}
        name={selectedRadioStationId !== null
          ? radioStations.find((s) => selectedRadioStationId === s.id).name : null}
      />
    </div>
  );
}
}

export default App;
