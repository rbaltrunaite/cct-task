import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import expect from 'expect';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Bottom from './Bottom/Bottom';
import Station from './Station/Station';
import Top from './Top/Top';
import App from './App';

configure({ adapter: new Adapter() });

describe('App', () => {
  let wrapper;
  const mockData = [{
    id: 5,
    name: 'Putin',
    frequency: 66.6,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  }];

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  beforeEach(() => {
    const mock = new MockAdapter(axios);
    mock
      .onGet(process.env.REACT_APP_RADIO_STATIONS)
      .reply(200, mockData);
  });

  it('renders without crashing', () => {
    shallow(<App />);
  });

  it('checks if "Top", wrapping component and "Bottom" is rendered', () => {
    const topComponent = wrapper.find(Top);
    expect(topComponent).toHaveLength(1);

    const wrappingComponent = wrapper.find('[data-test=\'wrappingComponent\']');
    expect(wrappingComponent).toHaveLength(1);

    const bottomComponent = wrapper.find(Bottom);
    expect(bottomComponent).toHaveLength(1);
  });

  describe('Have NO "Radio station" objects', () => {
    it('should not render any "Station" components', () => {
      const radioStations = [];

      wrapper.setState({ radioStations }, () => {
        expect(wrapper.find(Station)).toHaveLength(0);
      });
    });
  });
});
