import React from 'react';
import PropTypes from 'prop-types';

/**
 * bottom To display selected station in the bottom of the App
 * @param {Object} props - Object of name of the station selected
 * @param {Boolean} props.show - Property to show or not to show selected radio station
 * @param {String} props.name - Name of the station selected
 */
const bottom = (props) => {
  const { show, name } = props;

  if (show) {
    return (
      <div className="bottom">
        <div className="row text-center">
          <span className="text-uppercase currently-playing col-12 mt-4">Currently playing</span>
          <span className="station col-12" data-test="nameOfTheSelectedStation">
            {name}
            {' '}
            FM
          </span>
        </div>
      </div>
    );
  }
  return (
    <div className="bottom" />
  );
};

bottom.propTypes = {
  name: PropTypes.string,
  show: PropTypes.bool,
};

bottom.defaultProps = {
  show: false,
  name: null,
};

export default bottom;
