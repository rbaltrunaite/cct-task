import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import expect from 'expect';
import Bottom from './Bottom';

configure({ adapter: new Adapter() });

describe('<Bottom/>', () => {
  it('should render name passed when "show" is "true"', () => {
    const propsWhenShowIsTrue = {
      show: true,
      name: 'test',
    };

    const wrapper = shallow(<Bottom {...propsWhenShowIsTrue} />);

    const nameOfTheSelectedStation = wrapper.find('[data-test=\'nameOfTheSelectedStation\']');

    expect(nameOfTheSelectedStation.text()).toEqual('test FM');
  });

  it('should render only "FM", then name is empty, but "show" is "true"', () => {
    const propsWhenNameIsEmpty = {
      show: true,
      name: '',
    };

    const wrapper = shallow(<Bottom {...propsWhenNameIsEmpty} />);
    const nameOfTheSelectedStation = wrapper.find('[data-test=\'nameOfTheSelectedStation\']');

    expect(nameOfTheSelectedStation.text()).toEqual(' FM');
  });

  it('should render no content when "show" is "false"', () => {
    const propsWhenShowIsFalse = {
      show: false,
      name: 'test',
    };

    const wrapper = shallow(<Bottom {...propsWhenShowIsFalse} />);

    expect(wrapper.find(Bottom).exists()).toEqual(false);
  });
});
