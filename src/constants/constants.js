const RADIO_STATIONS = [
  {
    id: 0,
    name: 'Putin',
    frequency: 66.6,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  },
  {
    id: 1,
    name: 'Dribbble',
    frequency: 101.2,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  },
  {
    id: 2,
    name: 'Doge',
    frequency: 99.4,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  },
  {
    id: 3,
    name: 'Ballads',
    frequency: 87.1,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  },
  {
    id: 4,
    name: 'Maximum',
    frequency: 142.2,
    img: 'https://i.ibb.co/0Gqj4Fx/image.jpg',
  },
];

export default RADIO_STATIONS;
