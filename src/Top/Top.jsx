import React from 'react';
import backArrowButton from '../images/back-arrow.png';
import switchButton from '../images/switch.png';

/**
 * top Renders not dynamic content: title and buttons
 */
const top = () => (
  <div className="top row w-100 mx-0 py-4">
    <div className="col-3 pl-3 button">
      <img src={backArrowButton} alt="back arrow" />
    </div>
    <p className="text-uppercase col-6 text-center my-auto">Stations</p>
    <div className="col-3 text-right pr-4 button">
      <img src={switchButton} alt="switch button" />
    </div>
  </div>
);

export default top;
