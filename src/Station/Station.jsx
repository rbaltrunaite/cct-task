import React from 'react';
import PropTypes from 'prop-types';

import minusButton from '../images/minus.png';
import plusButton from '../images/plus.png';

/**
 * Station
 * Individual parameters added according to props received
 * @param {Object} props - All information of the station
 * @param {String} props.name - Name of the radio station
 * @param {String} props.img - Image of the radio station
 * @param {Number} props.frequency - Frequency of the radio station
 * @param {Function} props.onClick - Calling selectRadioStation to change current radio station
 * @param {Boolean} props.show - Show or not image of the radio station
 */
const station = (props) => {
  const {
    img, frequency, name, show, onClick,
  } = props;
  let frequencyWithComma = frequency.toString();
  frequencyWithComma = frequencyWithComma.replace('.', ',');

  const radioStationImageContainer = (
    <div className="image row mx-0 mb-4 mt-1" data-test="radioStationImageContainer">
      <div className="col-3 text-center button">
        <img src={minusButton} alt="minus button" className="py-auto" />
      </div>
      <div className="col-6 text-center">
        <img src={img} alt="radio station" className="picture" />
      </div>
      <div className="col-3 text-center button">
        <img src={plusButton} alt="plus button" />
      </div>
    </div>
  );

  const radioStationButtonContainer = (
    <button onClick={onClick} className="col-12 py-2 px-0 mx-auto pb-3" data-test="radioStationButtonContainer" type="button">
      <span className="float-left">
        {name}
        {' '}
        FM
      </span>
      <span className="float-right">{frequencyWithComma}</span>
    </button>
  );

  if (show) {
    return (
      <div className="mx-auto py-2 station">
        {radioStationImageContainer}
        {radioStationButtonContainer}
      </div>
    );
  }
  return (
    <div className="mx-auto py-2 station">
      {radioStationButtonContainer}
    </div>
  );
};

station.propTypes = {
  name: PropTypes.string.isRequired,
  show: PropTypes.bool,
  onClick: PropTypes.func,
  frequency: PropTypes.number.isRequired,
  img: PropTypes.string.isRequired,
};

station.defaultProps = {
  show: false,
  onClick: null,
};

export default station;
