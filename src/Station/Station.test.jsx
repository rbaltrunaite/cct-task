import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import expect from 'expect';
import Station from './Station';

configure({ adapter: new Adapter() });

const props = {
  name: 'test',
  frequency: 123,
  key: 123,
  img: 'test',
};

const wrapper = shallow(<Station {...props} />);

describe('<Station/>', () => {
  it('should render only "radioStationButtonContainer" when "show" prop is "false"', () => {
    expect(wrapper.find('[data-test=\'radioStationButtonContainer\']')).toHaveLength(1);
    expect(wrapper.find('[data-test=\'radioStationImageContainer\']')).toHaveLength(0);
  });

  it('should render both: "radioStationButtonContainer" and "radioStationImageContainer" when "show" prop is "true"', () => {
    wrapper.setProps({ show: true });

    expect(wrapper.find('[data-test=\'radioStationButtonContainer\']')).toHaveLength(1);
    expect(wrapper.find('[data-test=\'radioStationImageContainer\']')).toHaveLength(1);
  });
});
